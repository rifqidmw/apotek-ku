package com.skripsi.apotekku.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.skripsi.apotekku.R;
import com.skripsi.apotekku.model.ModelMedicine;
import com.skripsi.apotekku.module.DetailMedicineActivity;
import com.skripsi.apotekku.module.DetailPaymentActivity;


import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MedicineAdapter extends RecyclerView.Adapter<MedicineAdapter.MyViewHolder> {
    private Context mContext;
    private List<ModelMedicine> medicineList;
    private View itemView;
    private String status;
    public static ProgressDialog mProgressDialog;


    public MedicineAdapter(Context mContext, List<ModelMedicine> medicineList, String status) {
        this.mContext = mContext;
        this.medicineList = medicineList;
        this.status = status;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_medicine, parent, false);

        return new MedicineAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final ModelMedicine medicine = medicineList.get(position);
        holder.tvName.setText(medicine.getName());
        holder.tvDate.setText(medicine.getDate());
        holder.tvId.setText(medicine.getId_medicine());
        if (status.equals("1")){
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mContext, DetailMedicineActivity.class);
                    intent.putExtra("ID", medicine.getId());
                    intent.putExtra("ID_MEDICINE", medicine.getId_medicine());
                    intent.putExtra("NAME", medicine.getName());
                    intent.putExtra("DATE", medicine.getDate());
                    intent.putExtra("BARCODE", medicine.getBarcode());
                    intent.putExtra("QTY", medicine.getQty());
                    mContext.startActivity(intent);
                }
            });
        } else if (status.equals("2")){
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Dialog dialog1 = new Dialog(mContext);
                    dialog1.setContentView(R.layout.approve_dialog);
                    dialog1.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                    dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                    Button btn_no, btn_yes;

                    btn_no = dialog1.findViewById(R.id.button_no);
                    btn_yes = dialog1.findViewById(R.id.button_gallery);

                    btn_no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog1.dismiss();
                        }
                    });

                    btn_yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mContext.startActivity(new Intent(mContext, DetailPaymentActivity.class));
                            dialog1.dismiss();
                        }
                    });
                    dialog1.show();
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return Math.min(medicineList.size(), 7);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName, tvId, tvDate;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.text_name);
            tvDate = itemView.findViewById(R.id.text_date);
            tvId = itemView.findViewById(R.id.text_id);
        }
    }

    public void setFilter(List<ModelMedicine> newList){
        medicineList=new ArrayList<>();
        medicineList.addAll(newList);
        notifyDataSetChanged();
    }
}
