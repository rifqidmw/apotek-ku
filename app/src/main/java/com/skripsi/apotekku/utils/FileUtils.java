package com.skripsi.apotekku.utils;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import androidx.core.content.FileProvider;

import com.skripsi.apotekku.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;

import static com.skripsi.apotekku.utils.LogUtils.LOGD;
import static com.skripsi.apotekku.utils.LogUtils.LOGE;


public class FileUtils {

    private static final String extensions[] = new String[]{"avi", "3gp", "mp4", "mp3", "jpeg", "jpg",
            "gif", "png",
            "pdf", "docx", "doc", "xls", "xlsx", "csv", "ppt", "pptx",
            "txt", "zip", "rar"};


    public static void openFile(Context context, File url) throws ActivityNotFoundException,
            IOException {
        Uri uri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".fileprovider", url);

        String urlString = url.toString().toLowerCase();

        Intent intent = new Intent(Intent.ACTION_VIEW);

        List<ResolveInfo> resInfoList = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        for (ResolveInfo resolveInfo : resInfoList) {
            String packageName = resolveInfo.activityInfo.packageName;
            context.grantUriPermission(packageName, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
        if (urlString.toLowerCase().toLowerCase().contains(".doc")
                || urlString.toLowerCase().contains(".docx")) {
            intent.setDataAndType(uri, "application/msword");
        } else if (urlString.toLowerCase().contains(".pdf")) {
            intent.setDataAndType(uri, "application/pdf");
        } else if (urlString.toLowerCase().contains(".ppt")
                || urlString.toLowerCase().contains(".pptx")) {
            intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
        } else if (urlString.toLowerCase().contains(".xls")
                || urlString.toLowerCase().contains(".xlsx")) {
            intent.setDataAndType(uri, "application/vnd.ms-excel");
        } else if (urlString.toLowerCase().contains(".zip")
                || urlString.toLowerCase().contains(".rar")) {
            intent.setDataAndType(uri, "application/trap");
        } else if (urlString.toLowerCase().contains(".rtf")) {
            intent.setDataAndType(uri, "application/rtf");
        } else if (urlString.toLowerCase().contains(".wav")
                || urlString.toLowerCase().contains(".mp3")) {
            intent.setDataAndType(uri, "audio/*");
        } else if (urlString.toLowerCase().contains(".gif")) {
            intent.setDataAndType(uri, "image/gif");
        } else if (urlString.toLowerCase().contains(".jpg")
                || urlString.toLowerCase().contains(".jpeg")
                || urlString.toLowerCase().contains(".png")) {
            intent.setDataAndType(uri, "image/jpeg");
        } else if (urlString.toLowerCase().contains(".txt")) {
            intent.setDataAndType(uri, "text/plain");
        } else if (urlString.toLowerCase().contains(".3gp")
                || urlString.toLowerCase().contains(".mpg")
                || urlString.toLowerCase().contains(".mpeg")
                || urlString.toLowerCase().contains(".mpe")
                || urlString.toLowerCase().contains(".mp4")
                || urlString.toLowerCase().contains(".avi")) {
            intent.setDataAndType(uri, "video/*");
        } else {
            intent.setDataAndType(uri, "*/*");
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
    public static String getAppPath(Context context) {
        File dir = new File(android.os.Environment.getExternalStorageDirectory()
                + File.separator
                + context.getResources().getString(R.string.app_name)
                + File.separator);
        if (!dir.exists()) {
            dir.mkdir();
        }
        return dir.getPath() + File.separator;
    }
    public static void copy(File src, File dst) {
        InputStream in;
        OutputStream out;
        try {
            in = new FileInputStream(src);
            out = new FileOutputStream(dst);

            String tempExt = FileUtils.getExtension(dst.getPath());

            if (tempExt.equals("jpeg") || tempExt.equals("jpg") || tempExt.equals("gif")
                    || tempExt.equals("png")) {
                if (out != null) {

                    Bitmap bit = BitmapFactory.decodeFile(src.getPath());
                    LOGD("Bitmap : " + bit);

                    if (bit.getWidth() > 700) {
                        if (bit.getHeight() > 700)
                            bit = Bitmap.createScaledBitmap(bit, 700, 700, true);
                        else
                            bit = Bitmap.createScaledBitmap(bit, 700, bit.getHeight(), true);
                    } else {
                        if (bit.getHeight() > 700)
                            bit = Bitmap.createScaledBitmap(bit, bit.getWidth(), 700, true);
                        else
                            bit = Bitmap.createScaledBitmap(bit, bit.getWidth(), bit.getHeight(), true);
                    }

                    bit.compress(Bitmap.CompressFormat.JPEG, 90, out);
                }
                LOGD("File Compressed...");
            } else {
                byte[] buf = new byte[1024 * 4];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            }

            in.close();
            out.close();

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            LOGE("Compressing ERror :  " + e.getLocalizedMessage());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            LOGE("Compressing ERror IOE : " + e.getLocalizedMessage());
        } catch (Exception e) {
            // TODO: handle exception
            LOGE("Compressing ERror Other: " + e.getLocalizedMessage());
        }
    }

    public static void move(File src, File dst) {
        InputStream in;
        OutputStream out;
        try {
            in = new FileInputStream(src);
            out = new FileOutputStream(dst);

            String tempExt = FileUtils.getExtension(dst.getPath());

            if (tempExt.equals("jpeg") || tempExt.equals("jpg") || tempExt.equals("gif")
                    || tempExt.equals("png")) {
                if (out != null) {

                    Bitmap bit = BitmapFactory.decodeFile(src.getPath());
                    LOGD("Bitmap : " + bit);

                    if (bit.getWidth() > 700 || bit.getHeight() > 700) {
                        bit = Bitmap.createScaledBitmap(bit, 700, 700, true);
                    }
                    bit.compress(Bitmap.CompressFormat.JPEG, 90, out);
                }
                LOGD("File Compressed...");
            } else {

                // Transfer bytes from in to out
                byte[] buf = new byte[1024 * 4];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            }

            in.close();
            out.close();

            if (src.delete())
                LOGD("File Successfully Copied...");

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            LOGE("Compressing ERror :  " + e.getLocalizedMessage());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            LOGE("Compressing ERror IOE : " + e.getLocalizedMessage());
        } catch (Exception e) {
            // TODO: handle exception
            LOGE("Compressing ERror Other: " + e.getLocalizedMessage());
        }
    }
    public static boolean isValidExtension(String ext) {
        return Arrays.asList(extensions).contains(ext);

    }

    public static String getExtension(String path) {
        return path.contains(".") ? path.substring(path.lastIndexOf(".") + 1).toLowerCase() : "";
    }
}
