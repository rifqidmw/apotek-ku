package com.skripsi.apotekku.model;

public class ModelMedicine {
    private String id;
    private String id_medicine;
    private String name;
    private String date;
    private String barcode;
    private String qty;

    public ModelMedicine() {
    }

    public ModelMedicine(String id, String id_medicine, String name, String date, String barcode, String qty) {
        this.id = id;
        this.id_medicine = id_medicine;
        this.name = name;
        this.date = date;
        this.barcode = barcode;
        this.qty = qty;
    }

    public String getId_medicine() {
        return id_medicine;
    }

    public void setId_medicine(String id_medicine) {
        this.id_medicine = id_medicine;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }
}
