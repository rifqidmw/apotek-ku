package com.skripsi.apotekku.module;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.skripsi.apotekku.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class AddMedicineActivity extends AppCompatActivity {

    ImageView btnCalendar, btnScan;
    EditText etName, etDate, etBarcode, etId, etQty;
    Button btnAdd;
    DatePickerDialog datePickerDialog;
    SimpleDateFormat dateFormatter;
    Toolbar toolbar;
    FirebaseFirestore firestore;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_medicine);

        btnCalendar = findViewById(R.id.btn_calendar);
        btnScan = findViewById(R.id.btn_scan);
        btnAdd = findViewById(R.id.button_add);
        etId = findViewById(R.id.et_id);
        etName = findViewById(R.id.et_name);
        etDate = findViewById(R.id.et_date);
        etBarcode = findViewById(R.id.et_barcode);
        etQty = findViewById(R.id.et_qty);
        toolbar = findViewById(R.id.toolbar);
        progressBar = findViewById(R.id.progressBar);

        FirebaseApp.initializeApp(this);
        firestore = FirebaseFirestore.getInstance();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AddMedicineActivity.this, MainActivity.class));
            }
        });

        Intent intent = getIntent();
        if (intent.getIntExtra("STATUS", 0) == 2){
            etId.setText(intent.getStringExtra("ID_MEDICINE"));
            etName.setText(intent.getStringExtra("NAME"));
            etDate.setText(intent.getStringExtra("DATE"));
            etBarcode.setText(intent.getStringExtra("BARCODE"));
            etQty.setText(intent.getStringExtra("QTY"));
        }

        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        btnCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDateDialog();
            }
        });
        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AddMedicineActivity.this, ScanActivity.class);
                if (!etId.getText().toString().equals("")){
                    intent.putExtra("ID_MEDICINE", etId.getText().toString());
                } else {
                    intent.putExtra("ID_MEDICINE", "");
                }
                if (!etName.getText().toString().equals("")){
                    intent.putExtra("NAME", etName.getText().toString());
                } else {
                    intent.putExtra("NAME", "");
                }
                if (!etDate.getText().toString().equals("")){
                    intent.putExtra("DATE", etDate.getText().toString());
                } else {
                    intent.putExtra("DATE", "");
                }
                if (!etId.getText().toString().equals("")){
                    intent.putExtra("QTY", etQty.getText().toString());
                } else {
                    intent.putExtra("QTY", "");
                }
                startActivity(intent);
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                btnAdd.setVisibility(View.INVISIBLE);

                String id = firestore.collection("medicine").document().getId();

                Map data = new HashMap<>();
                data.put("id", id);
                data.put("id_medicine", etId.getText().toString());
                data.put("name", etName.getText().toString());
                data.put("expired", etDate.getText().toString());
                data.put("barcode", etBarcode.getText().toString());
                data.put("qty", etQty.getText().toString());

                if (!etId.getText().toString().equals("") && !etName.getText().toString().equals("") && !etDate.getText().toString().equals("") && !etBarcode.getText().toString().equals("")){
                    firestore.collection("medicine").document(id).set(data).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            DocumentReference viewCount = firestore.collection("medCount").document(getApplicationContext().getString(R.string.count_id));
                            viewCount.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                    int count;
                                    DocumentSnapshot docSnapshot = task.getResult();
                                    count = Integer.parseInt(docSnapshot.getString("medicine_count"));
                                    count += 1;
                                    DocumentReference updateCount = firestore.collection("medCount").document(getApplicationContext().getString(R.string.count_id));
                                    updateCount.update("medicine_count", Integer.toString(count)).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            Toast.makeText(AddMedicineActivity.this, "Add Medicine Success", Toast.LENGTH_SHORT).show();
                                            startActivity(new Intent(AddMedicineActivity.this, MainActivity.class));
                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            progressBar.setVisibility(View.INVISIBLE);
                                            btnAdd.setVisibility(View.VISIBLE);
                                        }
                                    });
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    progressBar.setVisibility(View.INVISIBLE);
                                    btnAdd.setVisibility(View.VISIBLE);
                                    Toast.makeText(AddMedicineActivity.this, "Error while adding the medicine : "+e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressBar.setVisibility(View.INVISIBLE);
                            btnAdd.setVisibility(View.VISIBLE);
                            Toast.makeText(AddMedicineActivity.this, "Error while adding the medicine : "+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {


                    progressBar.setVisibility(View.INVISIBLE);
                    btnAdd.setVisibility(View.VISIBLE);
                    Toast.makeText(AddMedicineActivity.this, "Adding medicine fail", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void showDateDialog(){
        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                etDate.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }
}
