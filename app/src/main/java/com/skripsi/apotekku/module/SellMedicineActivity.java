package com.skripsi.apotekku.module;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.skripsi.apotekku.R;
import com.skripsi.apotekku.adapter.MedicineAdapter;
import com.skripsi.apotekku.model.ModelMedicine;
import com.skripsi.apotekku.utils.Api;

import java.util.ArrayList;
import java.util.List;

import info.androidhive.barcode.BarcodeReader;

public class SellMedicineActivity extends AppCompatActivity implements BarcodeReader.BarcodeReaderListener  {

    private BottomSheetBehavior sheetBehavior;
    private ConstraintLayout bottom_sheet;
    private BarcodeReader barcodeReader;
    private MedicineAdapter medicineAdapter;
    private RecyclerView recyclerView;
    private FirebaseFirestore firestore;
    private Toolbar toolbar;

    private EditText etName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sell_medicine);

        etName = findViewById(R.id.et_medicine_name);
        bottom_sheet = findViewById(R.id.bottom_sheet);
        sheetBehavior = BottomSheetBehavior.from(bottom_sheet);
        barcodeReader = (BarcodeReader) getSupportFragmentManager().findFragmentById(R.id.barcode_fragment);
        toolbar = findViewById(R.id.toolbar);
        recyclerView = findViewById(R.id.recycler_view);

        FirebaseApp.initializeApp(this);
        firestore = FirebaseFirestore.getInstance();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Api.medicineList = new ArrayList<>();
        medicineAdapter = new MedicineAdapter(this, Api.medicineList, "2");
        final LinearLayoutManager mLayout = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        mLayout.setReverseLayout(false);
        mLayout.setStackFromEnd(false);
        recyclerView.setLayoutManager(mLayout);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(medicineAdapter);

        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {

                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:

                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });

        etName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String textSearch = editable.toString();
                textSearch=textSearch.toLowerCase();
                List<ModelMedicine> newList=new ArrayList<>();
                if (textSearch.isEmpty()){
                    newList = Api.medicineList;
                }else {
                    for (ModelMedicine medicineList : Api.medicineList){
                        String title=medicineList.getName().toLowerCase();
                        if (title.contains(textSearch)){
                            newList.add(medicineList);
                        }
                    }
                }
                medicineAdapter.setFilter(newList);
            }
        });

        getData();
    }

    private void getData(){
        firestore.collection("medicine")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        Api.medicineList.clear();
                        for (DocumentSnapshot doc: task.getResult()){
                            ModelMedicine modelMedicine = new ModelMedicine();
                            modelMedicine.setId(doc.getString("id"));
                            modelMedicine.setId_medicine(doc.getString("id_medicine"));
                            modelMedicine.setName(doc.getString("name"));
                            modelMedicine.setDate(doc.getString("expired"));
                            modelMedicine.setBarcode(doc.getString("barcode"));
                            modelMedicine.setQty(doc.getString("qty"));
                            Log.d("name", doc.getString("name"));
                            Api.medicineList.add(modelMedicine);
                        }

                        medicineAdapter = new MedicineAdapter(SellMedicineActivity.this, Api.medicineList, "2");
                        medicineAdapter.notifyDataSetChanged();
                        recyclerView.setAdapter(medicineAdapter);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
    }

    @Override
    public void onScanned(Barcode barcode) {
        startActivity(new Intent(SellMedicineActivity.this, DetailPaymentActivity.class));
    }

    @Override
    public void onScannedMultiple(List<Barcode> barcodes) {

    }

    @Override
    public void onBitmapScanned(SparseArray<Barcode> sparseArray) {

    }

    @Override
    public void onScanError(String errorMessage) {

    }

    @Override
    public void onCameraPermissionDenied() {

    }
}
