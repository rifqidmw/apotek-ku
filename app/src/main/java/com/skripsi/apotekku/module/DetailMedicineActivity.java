package com.skripsi.apotekku.module;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.skripsi.apotekku.R;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class DetailMedicineActivity extends AppCompatActivity {

    private EditText etID, etName, etDate, etBarcode, etQty;
    private Button btnUpdate;
    private ImageView btnDelete;
    ProgressBar progressBar;
    Toolbar toolbar;

    FirebaseFirestore firestore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_medicine);

        FirebaseApp.initializeApp(this);
        firestore = FirebaseFirestore.getInstance();

        etID = findViewById(R.id.et_id);
        etName = findViewById(R.id.et_name);
        etDate = findViewById(R.id.et_date);
        etBarcode = findViewById(R.id.et_barcode);
        etQty = findViewById(R.id.et_qty);
        btnUpdate = findViewById(R.id.button_update);
        btnDelete = findViewById(R.id.btn_delete);
        progressBar = findViewById(R.id.progressBar);
        toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DetailMedicineActivity.this, MainActivity.class));
            }
        });

        final Intent intent = getIntent();
        etID.setText(intent.getStringExtra("ID_MEDICINE"));
        etName.setText(intent.getStringExtra("NAME"));
        etDate.setText(intent.getStringExtra("DATE"));
        etBarcode.setText(intent.getStringExtra("BARCODE"));
        etQty.setText(intent.getStringExtra("QTY"));

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                btnUpdate.setVisibility(View.INVISIBLE);
                DocumentReference updateCount = firestore.collection("medicine").document(intent.getStringExtra("ID"));
                updateCount.update(
                        "id_medicine", etID.getText().toString(),
                        "name", etName.getText().toString(),
                        "expired", etDate.getText().toString(),
                        "barcode", etBarcode.getText().toString(),
                        "qty", etQty.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(DetailMedicineActivity.this, "Update Medicine Success", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(DetailMedicineActivity.this, MainActivity.class));
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        progressBar.setVisibility(View.INVISIBLE);
                        btnUpdate.setVisibility(View.VISIBLE);
                    }
                });
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog1 = new Dialog(DetailMedicineActivity.this);
                dialog1.setContentView(R.layout.delete_dialog);
                dialog1.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                Button btn_no, btn_yes;

                btn_no = dialog1.findViewById(R.id.button_no);
                btn_yes = dialog1.findViewById(R.id.button_gallery);

                btn_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog1.dismiss();
                    }
                });

                btn_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DocumentReference deleteItem = firestore.collection("medicine").document(intent.getStringExtra("ID"));
                        deleteItem.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                Toast.makeText(DetailMedicineActivity.this, "Delete Medicine Success", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(DetailMedicineActivity.this, MainActivity.class));
                                dialog1.dismiss();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                dialog1.dismiss();

                            }
                        });
                    }
                });
                dialog1.show();
            }
        });
    }
}
