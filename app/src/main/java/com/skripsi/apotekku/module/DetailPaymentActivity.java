package com.skripsi.apotekku.module;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.skripsi.apotekku.R;
import com.skripsi.apotekku.adapter.CartAdapter;
import com.skripsi.apotekku.adapter.MedicineAdapter;
import com.skripsi.apotekku.model.ModelMedicine;
import com.skripsi.apotekku.utils.Api;

import java.util.ArrayList;

public class DetailPaymentActivity extends AppCompatActivity {


    private CartAdapter cartAdapter;
    private RecyclerView recyclerView;
    private FirebaseFirestore firestore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_payment);

        recyclerView = findViewById(R.id.recycler_view);

        FirebaseApp.initializeApp(this);
        firestore = FirebaseFirestore.getInstance();

        Api.medicineList = new ArrayList<>();
        cartAdapter = new CartAdapter(this, Api.medicineList, "2");
        final LinearLayoutManager mLayout = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        mLayout.setReverseLayout(false);
        mLayout.setStackFromEnd(false);
        recyclerView.setLayoutManager(mLayout);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(cartAdapter);

        getData();
    }

    private void getData() {
        firestore.collection("medicine")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        Api.medicineList.clear();
                        for (DocumentSnapshot doc : task.getResult()) {
                            ModelMedicine modelMedicine = new ModelMedicine();
                            modelMedicine.setId(doc.getString("id"));
                            modelMedicine.setName(doc.getString("name"));
                            modelMedicine.setDate(doc.getString("expired"));
                            modelMedicine.setBarcode(doc.getString("barcode"));
                            Log.d("name", doc.getString("name"));
                            Api.medicineList.add(modelMedicine);
                        }

                        cartAdapter = new CartAdapter(DetailPaymentActivity.this, Api.medicineList, "2");
                        cartAdapter.notifyDataSetChanged();
                        recyclerView.setAdapter(cartAdapter);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
    }
}
