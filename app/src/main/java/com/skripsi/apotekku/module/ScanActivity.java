package com.skripsi.apotekku.module;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;

import com.google.android.gms.vision.barcode.Barcode;
import com.skripsi.apotekku.R;

import java.util.List;

import info.androidhive.barcode.BarcodeReader;

public class ScanActivity extends AppCompatActivity implements BarcodeReader.BarcodeReaderListener  {

    private BarcodeReader barcodeReader;
    private String name, date, id, qty;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        barcodeReader = (BarcodeReader) getSupportFragmentManager().findFragmentById(R.id.barcode_fragment);
        toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Intent intent = getIntent();
        if (intent.getStringExtra("NAME") != ""){
            name = intent.getStringExtra("NAME");
        }
        if (intent.getStringExtra("DATE") != ""){
            date = intent.getStringExtra("DATE");
        }
        if (intent.getStringExtra("ID_MEDICINE") != ""){
            id = intent.getStringExtra("ID_MEDICINE");
        }
        if (intent.getStringExtra("QTY") != ""){
            qty = intent.getStringExtra("QTY");
        }
    }

    @Override
    public void onScanned(Barcode barcode) {
        barcodeReader.playBeep();
        Intent intent = new Intent(ScanActivity.this, AddMedicineActivity.class);
        intent.putExtra("BARCODE", barcode.displayValue);
        intent.putExtra("ID_MEDICINE", id);
        intent.putExtra("NAME", name);
        intent.putExtra("DATE", date);
        intent.putExtra("QTY", qty);
        intent.putExtra("STATUS", 2);
        startActivity(intent);
    }



    @Override
    public void onScannedMultiple(List<Barcode> barcodes) {

    }

    @Override
    public void onBitmapScanned(SparseArray<Barcode> sparseArray) {

    }

    @Override
    public void onScanError(String errorMessage) {

    }

    @Override
    public void onCameraPermissionDenied() {

    }
}
