package com.skripsi.apotekku.module;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.skripsi.apotekku.R;
import com.skripsi.apotekku.adapter.MedicineAdapter;
import com.skripsi.apotekku.model.ModelMedicine;
import com.skripsi.apotekku.utils.Api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    Button btnAddMed, btnSellMed;
    ImageView btnReport;
    TextView tvCount;

    FirebaseFirestore firestore;

    private MedicineAdapter medicineAdapter;
    private RecyclerView recyclerView;

    private static final int PERMISSION_REQUEST_CODE = 1100;
    String[] appPermission = {
            Manifest.permission.CAMERA,
            Manifest.permission.INTERNET,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnAddMed = findViewById(R.id.button_add_medicine);
        btnSellMed = findViewById(R.id.button_sell_medicine);
        btnReport = findViewById(R.id.btn_report);
        tvCount = findViewById(R.id.txt_total);
        recyclerView = findViewById(R.id.recycler_medicine);
        FirebaseApp.initializeApp(this);
        firestore = FirebaseFirestore.getInstance();

        Api.medicineList = new ArrayList<>();
        medicineAdapter = new MedicineAdapter(this, Api.medicineList, "1");
        final LinearLayoutManager mLayout = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        mLayout.setReverseLayout(false);
        mLayout.setStackFromEnd(false);
        recyclerView.setLayoutManager(mLayout);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(medicineAdapter);

        btnAddMed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddMedicineActivity.class);
                intent.putExtra("STATUS", 1);
                startActivity(intent);
            }
        });
        btnSellMed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, SellMedicineActivity.class));
            }
        });
        btnReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        if (checkAndRequestPermission()){

        }

        getData();
    }

    private void getData(){
        firestore.collection("medicine")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        Api.medicineList.clear();
                        for (DocumentSnapshot doc: task.getResult()){
                            ModelMedicine modelMedicine = new ModelMedicine();
                            modelMedicine.setId(doc.getString("id"));
                            modelMedicine.setId_medicine(doc.getString("id_medicine"));
                            modelMedicine.setName(doc.getString("name"));
                            modelMedicine.setDate(doc.getString("expired"));
                            modelMedicine.setBarcode(doc.getString("barcode"));
                            modelMedicine.setQty(doc.getString("qty"));
                            Log.d("name", doc.getString("name"));
                            Api.medicineList.add(modelMedicine);
                        }

                        medicineAdapter = new MedicineAdapter(MainActivity.this, Api.medicineList, "1");
                        medicineAdapter.notifyDataSetChanged();
                        recyclerView.setAdapter(medicineAdapter);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });

        firestore.collection("medCount")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        for (DocumentSnapshot doc: task.getResult()){
                            tvCount.setText(doc.getString("medicine_count"));
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
    }

    public boolean checkAndRequestPermission(){
        List<String> listPermissionNeeded = new ArrayList<>();
        for (String perm: appPermission){
            if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED){
                listPermissionNeeded.add(perm);
            }
        }
        if (!listPermissionNeeded.isEmpty()){
            ActivityCompat.requestPermissions(this, listPermissionNeeded.toArray(new String[listPermissionNeeded.size()]), PERMISSION_REQUEST_CODE);
            return false;
        }

        return true;
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CODE){
            HashMap<String, Integer> permissionResults = new HashMap<>();
            int deniedCount = 0;

            for (int i=0; i<grantResults.length;i++){
                if (grantResults[i] == PackageManager.PERMISSION_DENIED){
                    permissionResults.put(permissions[i], grantResults[i]);
                    deniedCount++;
                }
            }

            if (deniedCount == 0){

            } else {
                for (Map.Entry<String, Integer> entry : permissionResults.entrySet()){
                    String permName = entry.getKey();
                    int permResult = entry.getValue();

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permName)){
                        AlertDialog.Builder dialog= new AlertDialog.Builder(this);
                        dialog.setTitle("Alert");
                        dialog.setMessage("This App need Camera Permission to work without and problems");
                        dialog.setPositiveButton("YES, Granted permission", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                checkAndRequestPermission();
                            }
                        });
                        dialog.setNegativeButton("No, Exit App", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                finish();
                                System.exit(0);
                            }
                        });
                        dialog.setCancelable(false);
                        AlertDialog alert = dialog.create();
                        alert.show();

                    }
                }
            }
        }
    }
}
